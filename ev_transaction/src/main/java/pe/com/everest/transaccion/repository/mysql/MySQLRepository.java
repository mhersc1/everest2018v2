package pe.com.everest.transaccion.repository.mysql;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.io.Serializable;

@NoRepositoryBean
interface MySQLRepository<T, ID extends Serializable> extends CrudRepository<T, ID> {

}





