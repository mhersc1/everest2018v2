package pe.com.everest.transaccion.component;

import pe.com.everest.transaccion.entity.Transaccion;

import java.util.List;

/**
 * Created by MHER on 8/18/2018.
 */
public abstract class ADT {
    public abstract Transaccion search(Transaccion trx);

    public abstract List<Transaccion> listar();

    public abstract void load(List<Transaccion> transacciones);
}
