package pe.com.everest.transaccion.component;

import pe.com.everest.transaccion.entity.Transaccion;

import java.util.LinkedList;
import java.util.List;

public class Queue extends ADT {
    private LinkedList<Transaccion> lista = new LinkedList<>();

    public Queue() {
    }

    public boolean isEmpty() {
        return lista.isEmpty();
    }

    public Transaccion dequeue() {
        return lista.removeFirst();
    }

    public void enqueue(Transaccion tx) {
        lista.addLast(tx);
    }

    public Transaccion peek() {
        return lista.peek();
    }

    public Transaccion search(Transaccion trxToSearch) {
        for (Transaccion trx : lista) {
            if (trx.getPeso() == trxToSearch.getPeso()) {
                return trx;
            }
        }
        return null;
    }

    public List<Transaccion> listar() {
        return this.lista;
    }

    @Override
    public void load(List<Transaccion> transacciones) {
        lista.clear();
        transacciones.forEach(this::enqueue);
    }

    public String toString() {
        return lista.toString();
    }
}