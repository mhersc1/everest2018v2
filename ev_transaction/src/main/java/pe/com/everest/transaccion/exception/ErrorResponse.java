package pe.com.everest.transaccion.exception;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by MHER on 8/21/2018.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ErrorResponse {
    private Boolean rpta;
    private String tipo;
    private String mensaje;

    public ErrorResponse(Boolean rpta, String mensaje) {
        this.rpta = rpta;
        this.mensaje = mensaje;
    }


    public Boolean getRpta() {
        return rpta;
    }

    public void setRpta(Boolean rpta) {
        this.rpta = rpta;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
}
