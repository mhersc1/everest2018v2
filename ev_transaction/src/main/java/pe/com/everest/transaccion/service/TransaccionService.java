
package pe.com.everest.transaccion.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.com.everest.transaccion.component.ADT;
import pe.com.everest.transaccion.component.ADTHandler;
import pe.com.everest.transaccion.component.ArbolBinario;
import pe.com.everest.transaccion.component.Queue;
import pe.com.everest.transaccion.dao.TransaccionDAO;
import pe.com.everest.transaccion.entity.ADTInstance;
import pe.com.everest.transaccion.entity.Transaccion;

import java.util.List;


@Service
public class TransaccionService {
    @Autowired
    TransaccionDAO transaccionDAO;

    @Autowired
    ADTHandler adtHandler;

    public ADTInstance crearADT(ADTInstance adtInstance) {
        ADTInstance newADT = transaccionDAO.saveADT(adtInstance);
        adtHandler.initializeADT(newADT.getId(), newADT.getTipo());
        return newADT;
    }

    public void registrarTrx(Transaccion transaccion) {
        ADT adt = adtHandler.getADT(transaccion.getAdtinstance());
        transaccionDAO.registrarTrx(transaccion);

        if (adt instanceof Queue) {
            ((Queue) adt).enqueue(transaccion);
            adtHandler.setADT(transaccion.getAdtinstance(), adt);
        }
        if (adt instanceof ArbolBinario) {
            ((ArbolBinario) adt).insert(transaccion);
            adtHandler.setADT(transaccion.getAdtinstance(), adt);
        }
    }

    public void eliminarTrx(Transaccion transaccion) {
        ADT adt = adtHandler.getADT(transaccion.getAdtinstance());
        transaccionDAO.eliminarTrx(transaccion);

        if (adt instanceof Queue) {
            ((Queue) adt).dequeue();
            adtHandler.setADT(transaccion.getAdtinstance(), adt);
        }

        if (adt instanceof ArbolBinario) {
            ((ArbolBinario) adt).eliminar(transaccion);
        }
    }

    public List<Transaccion> listarTrx(int adtInstance) {
        ADT adt = adtHandler.getADT(adtInstance);
        return adt.listar();
    }

    public Transaccion buscarTrx(Transaccion transaccion) {
        ADT adt = adtHandler.getADT(transaccion.getAdtinstance());
        return adt.search(transaccion);
    }

    public void fillADTMap() {
        adtHandler.fillADTMap(transaccionDAO.initialize());
    }

    public void registrarADTSeleccionada(String value) {
        transaccionDAO.registrarADTSeleccionado(value);
    }

    public Integer obtenerUltimoADTSeleccionado() {
        return transaccionDAO.obtenerUltimoADTSeleccionado();
    }
}