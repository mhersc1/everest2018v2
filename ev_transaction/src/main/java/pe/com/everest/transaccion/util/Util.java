package pe.com.everest.transaccion.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import pe.com.everest.transaccion.exception.TransaccionException;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Field;
import java.text.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.TextStyle;
import java.util.*;

public class Util {
    private static final String DATABASE = "app.trx.db";

    public static void injectDataBaseInRunTime(String value) throws Exception {
        injectEnvironmentVariable(DATABASE, value);
    }

    public static String getCurrentDataBase() {
        return System.getenv().get(DATABASE);
    }

    @SuppressWarnings("unchecked")
    private static void injectEnvironmentVariable(String key, String value)
            throws Exception {
        Class<?> processEnvironment = Class.forName("java.lang.ProcessEnvironment");
        Field unmodifiableMapField = getAccessibleField(processEnvironment, "theUnmodifiableEnvironment");
        Object unmodifiableMap = unmodifiableMapField.get(null);
        injectIntoUnmodifiableMap(key, value, unmodifiableMap);
        Field mapField = getAccessibleField(processEnvironment, "theEnvironment");
        Map<String, String> map = (Map<String, String>) mapField.get(null);
        map.put(key, value);
    }

    private static Field getAccessibleField(Class<?> clazz, String fieldName)
            throws NoSuchFieldException {
        Field field = clazz.getDeclaredField(fieldName);
        field.setAccessible(true);
        return field;
    }

    @SuppressWarnings("unchecked")
    private static void injectIntoUnmodifiableMap(String key, String value, Object map)
            throws ReflectiveOperationException {
        Class unmodifiableMap = Class.forName("java.util.Collections$UnmodifiableMap");
        Field field = getAccessibleField(unmodifiableMap, "m");
        Object obj = field.get(map);
        ((Map<String, String>) obj).put(key, value);
    }

    public static String printStackTrace(Exception ex) {
        StringWriter errors = new StringWriter();
        ex.printStackTrace(new PrintWriter(errors));
        return errors.toString();
    }

    public static String formatAsJson(Object object) throws Exception {
        ObjectMapper om = new ObjectMapper();
        return om.writeValueAsString(object);
    }


    public static String concatString(String[] arreglo, String sqlQuery) {
        String SQLQuery = MessageFormat.format(sqlQuery, arreglo);
        return SQLQuery;
    }

    public static String formatHHMM(Date date) {
        DateFormat dateFormat = new SimpleDateFormat("hh:mm a");
        return dateFormat.format(date);
    }

    public static String formatyyyyMMdd(Date date) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return dateFormat.format(date);
    }

    public static String formatyyyyMMddHHmmss(Date date) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return dateFormat.format(date);
    }

    public static Date obtainOnlyDate(String fecha) {
        try {
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            Date date = formatter.parse(fecha);
            return date;
        } catch (ParseException e) {
            e.printStackTrace();
            throw new TransaccionException("Fecha " + fecha + " no tiene formato yyyy-MM-dd");
        }
    }

    public static Date obtainFullDate(String fecha) {
        try {
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = formatter.parse(fecha);
            return date;
        } catch (ParseException e) {
            e.printStackTrace();
            throw new TransaccionException("Fecha " + fecha + " no tiene formato yyyy-MM-dd HH:mm:ss");
        }

    }

    public static String join(List<String> values) {
        return String.join(",", values);
    }

    public static String obtainDayOfWeek(Date date) {
        Locale locale = new Locale("es", "ES");
        LocalDate localDate = date.toInstant().atZone(ZoneId.of("America/Lima")).toLocalDate();
        return localDate.getDayOfWeek().getDisplayName(TextStyle.FULL, locale);
    }

    public static LocalTime obtainHour(String hora) {
        try {
            DateFormat formatter = new SimpleDateFormat("hh:mm a");
            Date date = formatter.parse(hora);
            LocalTime localTime = date.toInstant().atZone(ZoneId.of("America/Lima")).toLocalTime();
            return localTime;
        } catch (ParseException e) {
            e.printStackTrace();
            throw new TransaccionException("Ocurrio un error al parsear la hora: " + hora);
        }
    }

    public static LocalTime obtainHour(Date date) {
        return date.toInstant().atZone(ZoneId.of("America/Lima")).toLocalTime();
    }

    public static String stripAccents(String s) {
        s = Normalizer.normalize(s, Normalizer.Form.NFD);
        s = s.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
        return s;
    }

    public static <K, V extends Comparable<? super V>> Map<K, V> sortMapByValue(Map<K, V> unsortMap) {
        List<Map.Entry<K, V>> list =
                new LinkedList<>(unsortMap.entrySet());

        Collections.sort(list, Comparator.comparing(o -> (o.getValue())));

        Map<K, V> result = new LinkedHashMap<>();
        for (Map.Entry<K, V> entry : list) {
            result.put(entry.getKey(), entry.getValue());
        }

        return result;
    }
}
