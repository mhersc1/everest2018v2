package pe.com.everest.transaccion.entity;

import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MHER on 8/16/2018.
 */
@Document(collection = "adtinstance")
public class ADTInstanceMongo extends ADTInstance {
    private List<TransaccionMongo> transaccionList;

    public ADTInstanceMongo(Integer id, Integer tipo) {
        super(tipo);
        super.setId(id);
    }

    @Override
    public Integer getId() {
        return super.getId();
    }

    @Override
    public void setId(Integer id) {
        super.setId(id);
    }

    @Override
    public Integer getTipo() {
        return super.getTipo();
    }

    @Override
    public void setTipo(Integer tipo) {
        super.setTipo(tipo);
    }

    @Override
    public String getNombre() {
        return super.getNombre();
    }

    @Override
    public void setNombre(String nombre) {
        super.setNombre(nombre);
    }

    @Override
    public String toString() {
        return super.toString();
    }

    public List<TransaccionMongo> getTransaccionList() {
        if (transaccionList == null)
            transaccionList = new ArrayList<>();
        return transaccionList;
    }

    @Override
    public List<Transaccion> getTransacciones() {
        return new ArrayList<>(transaccionList);
    }
}
