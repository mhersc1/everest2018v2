package pe.com.everest.transaccion.repository.mongo;

import pe.com.everest.transaccion.entity.ADTInstanceMongo;

public interface ADTDocumentRepository extends MongoDBRepository<ADTInstanceMongo, Integer> {
}
