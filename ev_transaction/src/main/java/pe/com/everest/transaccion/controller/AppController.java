package pe.com.everest.transaccion.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pe.com.everest.transaccion.enume.DBEnum;
import pe.com.everest.transaccion.exception.TransaccionException;
import pe.com.everest.transaccion.service.TransaccionService;
import pe.com.everest.transaccion.util.Util;

import java.util.HashMap;
import java.util.Map;

import static pe.com.everest.transaccion.enume.MessageEnum.DB_NOT_ALLOWED;

/**
 * Created by MHER on 21/08/2018.
 */
@RestController
@RequestMapping("/app")
public class AppController {
    @Autowired
    private TransaccionService transaccionService;

    @GetMapping(value = "/configurarDatabase")
    String configureDatabase(@RequestParam String database) throws Exception {
        if (!DBEnum.verifyEnum(database))
            throw new TransaccionException(DB_NOT_ALLOWED.mensaje(database));
        Util.injectDataBaseInRunTime(database);
        transaccionService.fillADTMap();
        return "Se establecio la BD seleccionada como medio de almacenamiento. ";
    }

    @GetMapping(value = "/registrarADTSeleccionado")
    ResponseEntity<?> registrarADTSeleccionado(@RequestParam String value) {
        Map<String, Object> response = new HashMap<>();
        response.put("rpta", true);
        response.put("mensaje", "Operación Exitosa. ");
        transaccionService.registrarADTSeleccionada(value);
        return new ResponseEntity<Object>(response, HttpStatus.OK);
    }

    @GetMapping(value = "/obtenerUltimoADTSeleccionado")
    ResponseEntity<?> obtenerUltimoADTSeleccionado() {
        Map<String, Object> response = new HashMap<>();
        response.put("id", transaccionService.obtenerUltimoADTSeleccionado());
        return new ResponseEntity<Object>(response, HttpStatus.OK);
    }
}
