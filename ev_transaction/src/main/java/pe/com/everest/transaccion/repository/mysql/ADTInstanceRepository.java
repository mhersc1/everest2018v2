package pe.com.everest.transaccion.repository.mysql;

import pe.com.everest.transaccion.entity.ADTInstanceJPA;

public interface ADTInstanceRepository extends MySQLRepository<ADTInstanceJPA, Integer> {
}
