package pe.com.everest.transaccion.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by MHER on 21/08/2018.
 */
@Entity
@Table(name = "configuracion")
public class ConfiguracionJPA extends Configuracion {

    public ConfiguracionJPA(String clave, String valor) {
        super(clave, valor);
    }

    @Id
    @Override
    public String getClave() {
        return super.getClave();
    }

    @Override
    public void setClave(String clave) {
        super.setClave(clave);
    }

    @Override
    public String getValor() {
        return super.getValor();
    }

    @Override
    public void setValor(String valor) {
        super.setValor(valor);
    }
}
