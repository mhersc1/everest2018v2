package pe.com.everest.transaccion.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;
import pe.com.everest.transaccion.component.ADT;
import pe.com.everest.transaccion.component.ArbolBinario;
import pe.com.everest.transaccion.component.GenericBuilder;
import pe.com.everest.transaccion.component.Queue;
import pe.com.everest.transaccion.entity.*;
import pe.com.everest.transaccion.enume.ADTEnum;
import pe.com.everest.transaccion.enume.DBEnum;
import pe.com.everest.transaccion.exception.TransaccionException;
import pe.com.everest.transaccion.repository.mongo.ADTDocumentRepository;
import pe.com.everest.transaccion.repository.mongo.ConfigDocumentRepository;
import pe.com.everest.transaccion.repository.mysql.ADTInstanceRepository;
import pe.com.everest.transaccion.repository.mysql.ConfiguracionRepository;
import pe.com.everest.transaccion.repository.mysql.TransaccionRepository;
import pe.com.everest.transaccion.util.Util;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static pe.com.everest.transaccion.enume.MessageEnum.DB_NOT_ALLOWED;

@Repository
public class TransaccionDAO {
    private static final String ADT_SEQ_KEY = "adt";
    private static final String TRX_SEQ_KEY = "trx";
    private static final String ADT_LAST_SELECTED = "adt.last.selected";

    @Autowired
    private
    ADTInstanceRepository adtInstanceRepository;

    @Autowired
    private
    ADTDocumentRepository adtDocumentRepository;

    @Autowired
    private
    TransaccionRepository transaccionRepository;

    @Autowired
    private ConfiguracionRepository configuracionRepository;

    @Autowired
    private ConfigDocumentRepository configDocumentRepository;

    @Autowired
    private MongoOperations mongoOperation;

    @Autowired
    private MongoTemplate mongoTemplate;

    public ADTInstance saveADT(ADTInstance adtInstance) {
        DBEnum dbEnum = DBEnum.valueOf(Util.getCurrentDataBase());
        switch (dbEnum) {
            case MYSQL:
                ADTInstanceJPA adtInstanceJPA = new ADTInstanceJPA(adtInstance.getTipo());
                adtInstanceRepository.save(adtInstanceJPA);
                return adtInstanceJPA;
            case MONGO:
                ADTInstanceMongo adtInstanceMongo = prepareADTDocument(adtInstance);
                adtDocumentRepository.save(adtInstanceMongo);
                return adtInstanceMongo;
            default:
                throw new TransaccionException(DB_NOT_ALLOWED.mensaje(dbEnum));
        }
    }

    private ADTInstanceMongo prepareADTDocument(ADTInstance adtInstance) {
        Integer adtSequence = adtInstance.getId() == null || adtInstance.getId() <= 0
                ? getNextSequenceId(ADT_SEQ_KEY) : adtInstance.getId();

        return new ADTInstanceMongo(adtSequence, adtInstance.getTipo());
    }

    public void registrarTrx(Transaccion transaccion) {
        DBEnum dbEnum = DBEnum.valueOf(Util.getCurrentDataBase());
        switch (dbEnum) {
            case MYSQL:
                transaccionRepository.save(GenericBuilder
                        .of(TransaccionJPA::new)
                        .with(TransaccionJPA::setNombre, transaccion.getNombre())
                        .with(TransaccionJPA::setFecha, transaccion.getFecha())
                        .with(TransaccionJPA::setPeso, transaccion.getPeso())
                        .with(TransaccionJPA::setTipo, transaccion.getTipo())
                        .with(TransaccionJPA::setAdtinstance, transaccion.getAdtinstance())
                        .build());
                break;
            case MONGO:
                ADTInstanceMongo adtInstanceMongo = adtDocumentRepository.findOne(transaccion.getAdtinstance());
                validaSiPesoEsUnico(adtInstanceMongo.getTransaccionList(), transaccion);
                adtInstanceMongo.getTransaccionList().add(toMongoTrx(transaccion));
                adtDocumentRepository.save(adtInstanceMongo);
                break;
        }
    }

    public void eliminarTrx(Transaccion transaccion) {
        DBEnum dbEnum = DBEnum.valueOf(Util.getCurrentDataBase());
        switch (dbEnum) {
            case MYSQL:
                transaccionRepository.delete(transaccion.getId());
                break;
            case MONGO:
                Query query = Query.query(Criteria.where("_id").in(transaccion.getId()));
                Update update = new Update().pull("transaccionList", query);
                mongoTemplate.updateFirst(new Query(), update, ADTInstanceMongo.class);
                break;
        }
    }


    public Map<Integer, ADT> initialize() {
        Map<Integer, ADT> adtMap = new HashMap<>();
        DBEnum dbEnum = DBEnum.valueOf(Util.getCurrentDataBase());
        switch (dbEnum) {
            case MYSQL:
                adtInstanceRepository.findAll()
                        .forEach(a -> fillADTMapFromMySQL(adtMap, a));
                break;
            case MONGO:
                adtDocumentRepository.findAll()
                        .forEach(a -> fillADTMapFromMongo(adtMap, a));
                break;
        }
        return adtMap;
    }

    private Integer getNextSequenceId(String key) {
        Query query = new Query(Criteria.where("_id").is(key));
        Update update = new Update();
        update.inc("seq", 1);
        FindAndModifyOptions options = new FindAndModifyOptions();
        options.returnNew(true);
        SequenceId seqId = mongoOperation.findAndModify(query, update, options, SequenceId.class);
        if (seqId == null) {
            throw new TransaccionException("Unable to get sequence id for key : " + key);
        }
        return seqId.getSeq();
    }


    private void fillADTMapFromMySQL(Map<Integer, ADT> adtMap, ADTInstanceJPA a) {
        List<Transaccion> transaccionList = transaccionRepository
                .findByAdtinstance(a.getId())
                .stream().map(t -> (Transaccion) t)
                .collect(Collectors.toList());

        ADTEnum adtEnum = ADTEnum.getEnum(a.getTipo());
        switch (adtEnum) {
            case QUEUE:
                ADT queue = new Queue();
                queue.load(transaccionList);
                adtMap.put(a.getId(), queue);
                break;
            case BINARYTREE:
                ADT arbolBinario = new ArbolBinario();
                arbolBinario.load(transaccionList);
                adtMap.put(a.getId(), arbolBinario);
                break;
        }
    }

    public void registrarADTSeleccionado(String id) {
        DBEnum dbEnum = DBEnum.valueOf(Util.getCurrentDataBase());
        switch (dbEnum) {
            case MYSQL:
                configuracionRepository.save(new ConfiguracionJPA(ADT_LAST_SELECTED, id));
                break;
            case MONGO:
                configDocumentRepository.save(new ConfiguracionMongo(ADT_LAST_SELECTED, id));
                break;
        }
    }

    public Integer obtenerUltimoADTSeleccionado() {
        DBEnum dbEnum = DBEnum.valueOf(Util.getCurrentDataBase());
        Configuracion configuracion = new Configuracion();
        switch (dbEnum) {
            case MYSQL:
                configuracion = configuracionRepository.findOne(ADT_LAST_SELECTED);
                break;
            case MONGO:
                configuracion = configDocumentRepository.findOne(ADT_LAST_SELECTED);
                break;
        }
        return Integer.parseInt(configuracion.getValor());
    }

    private void fillADTMapFromMongo(Map<Integer, ADT> adtMap, ADTInstanceMongo a) {
        ADTEnum adtEnum = ADTEnum.getEnum(a.getTipo());
        switch (adtEnum) {
            case QUEUE:
                ADT queue = new Queue();
                queue.load(a.getTransacciones());
                adtMap.put(a.getId(), queue);
                break;
            case BINARYTREE:
                ADT arbolBinario = new ArbolBinario();
                arbolBinario.load(a.getTransacciones());
                adtMap.put(a.getId(), arbolBinario);
                break;
        }
    }

    private static void validaSiPesoEsUnico(List<TransaccionMongo> transacciones, Transaccion trx) {
        if (transacciones.stream().anyMatch(t -> t.getPeso().equals(trx.getPeso())))
            throw new TransaccionException("El peso " + trx.getPeso() + "ya existe.");
    }

    private TransaccionMongo toMongoTrx(Transaccion trx) {
        Integer trxSequence = trx.getId() == null || trx.getId() <= 0
                ? getNextSequenceId(TRX_SEQ_KEY) : trx.getId();

        return GenericBuilder.of(TransaccionMongo::new)
                .with(TransaccionMongo::setId, trxSequence)
                .with(TransaccionMongo::setFecha, new Date())
                .with(TransaccionMongo::setNombre, trx.getNombre())
                .with(TransaccionMongo::setTipo, trx.getTipo())
                .with(TransaccionMongo::setPeso, trx.getPeso())
                .build();
    }

}
