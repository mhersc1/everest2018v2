package pe.com.everest.transaccion.component;

import pe.com.everest.transaccion.entity.Transaccion;
import pe.com.everest.transaccion.exception.TransaccionException;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Stack;
import java.util.stream.Collectors;

public class ArbolBinario extends ADT {
    private Nodo root;

    public ArbolBinario() {
        root = null;
    }

    public void eliminar(Transaccion trx) {
        Nodo nodo, p = root, prev = null;
        while (p != null && !Objects.equals(p.transaccion.getPeso(), trx.getPeso())) {
            prev = p;
            if (p.transaccion.getPeso() < trx.getPeso())
                p = p.derecho;
            else
                p = p.izquierdo;
        }
        nodo = p;
        if (p != null && Objects.equals(p.transaccion.getPeso(), trx.getPeso())) {
            if (nodo.derecho == null)
                nodo = nodo.izquierdo;
            else if (nodo.izquierdo == null)
                nodo = nodo.derecho;
            else {
                Nodo tmp = nodo.izquierdo;
                Nodo previous = nodo;
                while (tmp.derecho != null) {
                    previous = tmp;
                    tmp = tmp.derecho;
                }
                nodo.transaccion.setPeso(tmp.transaccion.getPeso());
                if (previous == nodo)
                    previous.izquierdo = tmp.izquierdo;
                else
                    previous.derecho = tmp.izquierdo;
            }
            if (p == root)
                root = nodo;
            else if (prev != null) {
                if (prev.izquierdo == p)
                    prev.izquierdo = nodo;
                else prev.derecho = nodo;
            }
        } else if (root != null)
            throw new TransaccionException("Llave " + trx.getPeso() + "no esta en el arbol.");
        else throw new TransaccionException("El arbol esta vacio.");
    }

    public Transaccion search(Transaccion tx) {
        return search(root, tx);
    }

    @Override
    public List<Transaccion> listar() {
        return inorder().stream().map(n -> n.transaccion).collect(Collectors.toList());
    }

    @Override
    public void load(List<Transaccion> transacciones) {
        root = null;
        transacciones.forEach(this::insert);
    }

    private Transaccion search(Nodo nodo, Transaccion tx) {
        while (nodo != null)
            if (Objects.equals(tx.getPeso(), nodo.transaccion.getPeso()))
                return tx;
            else if (tx.getPeso() < (nodo.transaccion.getPeso()))
                nodo = nodo.izquierdo;
            else
                nodo = nodo.derecho;
        return null;
    }

    public void insert(Transaccion tx) {
        Nodo nodo = root, prev = null;
        while (nodo != null) {
            prev = nodo;
            if (nodo.transaccion.getPeso() < tx.getPeso())
                nodo = nodo.derecho;
            else nodo = nodo.izquierdo;
        }
        if (root == null)
            root = new Nodo(tx);
        else if (prev.transaccion.getPeso() < tx.getPeso())
            prev.derecho = new Nodo(tx);
        else prev.izquierdo = new Nodo(tx);
    }

    private List<Nodo> inorder() {
        List<Nodo> inOrderList = new LinkedList<>();
        Nodo p = root;
        Stack<Nodo> travStack = new Stack<>();
        while (p != null) {
            while (p != null) {
                if (p.derecho != null)
                    travStack.push(p.derecho);
                travStack.push(p);
                p = p.izquierdo;
            }
            p = travStack.pop();
            while (!travStack.isEmpty() && p.derecho == null) {
                inOrderList.add(p);
                p = travStack.pop();
            }
            inOrderList.add(p);
            if (!travStack.isEmpty())
                p = travStack.pop();
            else
                p = null;
        }
        return inOrderList;
    }
}

class Nodo {
    Transaccion transaccion;
    Nodo izquierdo, derecho;

    Nodo(Transaccion transaccion) {
        this.transaccion = transaccion;
        this.izquierdo = null;
        this.derecho = null;
    }
}
