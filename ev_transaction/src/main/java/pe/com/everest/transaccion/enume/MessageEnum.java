package pe.com.everest.transaccion.enume;

import java.text.MessageFormat;

/**
 * Created by MHER on 8/18/2018.
 */
public enum MessageEnum {
    DB_NOT_ALLOWED("La base de datos {0} no esta permitida."),
    ADT_NOT_ALLOWED("El identificador {0} no corresponde con una ADT configurada.");
    private String mensaje;

    MessageEnum(String mensaje) {
        this.mensaje = mensaje;
    }

    public String mensaje(Object... params) {
        return MessageFormat.format(mensaje, params);
    }
}
