package pe.com.everest.transaccion.repository.mongo;

import pe.com.everest.transaccion.entity.ConfiguracionMongo;

/**
 * Created by MHER on 21/08/2018.
 */
public interface ConfigDocumentRepository extends MongoDBRepository<ConfiguracionMongo, String> {
}
