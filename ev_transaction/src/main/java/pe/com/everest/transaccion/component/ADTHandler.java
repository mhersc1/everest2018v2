package pe.com.everest.transaccion.component;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;
import pe.com.everest.transaccion.enume.ADTEnum;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@Component(value = "adtHandler")
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS, value = WebApplicationContext.SCOPE_APPLICATION)
public class ADTHandler implements Serializable {
    private Map<Integer, ADT> adtMap;

    public ADTHandler() {
        this.adtMap = new HashMap<>();
    }

    public void setADT(Integer id, ADT adtInstance) {
        this.adtMap.put(id, adtInstance);
    }

    public void initializeADT(Integer id, Integer tipo) {
        switch (ADTEnum.getEnum(tipo)) {
            case QUEUE:
                this.adtMap.put(id, new Queue());
                break;
            case BINARYTREE:
                this.adtMap.put(id, new ArbolBinario());
                break;
        }
    }

    public ADT getADT(Integer id) {
        return this.adtMap.get(id);
    }

    public void fillADTMap(Map<Integer, ADT> adtMap) {
        this.adtMap.putAll(adtMap);
    }
}
