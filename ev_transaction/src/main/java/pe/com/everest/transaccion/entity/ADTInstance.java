package pe.com.everest.transaccion.entity;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ADTInstance implements Serializable {
    private Integer id;
    private Integer tipo;
    private String nombre;
    private List<Transaccion> transacciones;

    public ADTInstance(Integer tipo) {
        this.tipo = tipo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTipo() {
        return tipo;
    }

    public void setTipo(Integer tipo) {
        this.tipo = tipo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<Transaccion> getTransacciones() {
        if (transacciones == null)
            transacciones = new ArrayList<>();
        return transacciones;
    }

    @Override
    public String toString() {
        return "ADTInstance{" +
                "id=" + id +
                ", tipo=" + tipo +
                ", nombre='" + nombre + '\'' +
                '}';
    }
}
