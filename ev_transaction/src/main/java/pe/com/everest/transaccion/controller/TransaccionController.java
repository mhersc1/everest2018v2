package pe.com.everest.transaccion.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pe.com.everest.transaccion.entity.ADTInstance;
import pe.com.everest.transaccion.entity.Transaccion;
import pe.com.everest.transaccion.service.TransaccionService;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by MHER on 8/18/2018.
 */

@RestController
@RequestMapping("/trx")
public class TransaccionController {
    @Autowired
    private TransaccionService transaccionService;

    @PostMapping(value = "/crearADT")
    @ResponseBody
    ResponseEntity<?> crearADT(@RequestBody ADTInstance adtInstance) {
        Map<String, Object> response = new HashMap<>();
        response.put("adt", transaccionService.crearADT(adtInstance));
        return new ResponseEntity<Object>(response, HttpStatus.OK);
    }

    @PostMapping(value = "/registrarTrx")
    @ResponseBody
    ResponseEntity<?> registrarTrx(@RequestBody Transaccion transaccion) {
        Map<String, Object> response = new HashMap<>();
        response.put("rpta", true);
        transaccionService.registrarTrx(transaccion);
        return new ResponseEntity<Object>(response, HttpStatus.OK);
    }

    @DeleteMapping(value = "/eliminarTrx")
    @ResponseBody
    ResponseEntity<?> eliminarTrx(@RequestBody Transaccion transaccion) {
        Map<String, Object> response = new HashMap<>();
        response.put("rpta", true);
        transaccionService.eliminarTrx(transaccion);
        return new ResponseEntity<Object>(response, HttpStatus.OK);
    }

    @GetMapping(value = "/listarTrx")
    @ResponseBody
    ResponseEntity<?> listarTrx(@RequestParam Integer adtInstance) {
        Map<String, Object> response = new HashMap<>();
        response.put("listado", transaccionService.listarTrx(adtInstance));
        return new ResponseEntity<Object>(response, HttpStatus.OK);
    }

    @GetMapping(value = "/buscarTrx")
    @ResponseBody
    ResponseEntity<?> buscarTrx(@RequestBody Transaccion transaccion) {
        Map<String, Object> response = new HashMap<>();
        response.put("trx", transaccionService.buscarTrx(transaccion));
        return new ResponseEntity<Object>(response, HttpStatus.OK);
    }
}