package pe.com.everest.transaccion.exception;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by MHER on 09/09/2017.
 */
@ControllerAdvice
public class TransaccionExceptionHandler {

    @ExceptionHandler({TransaccionException.class})
    public @ResponseBody
    ErrorResponse handleNotaException(Exception ex) {
        return new ErrorResponse(false, ex.getMessage());
    }

}
