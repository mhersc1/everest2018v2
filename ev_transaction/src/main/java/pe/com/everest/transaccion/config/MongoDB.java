package pe.com.everest.transaccion.config;

import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import java.util.Arrays;

@Configuration
@PropertySource("classpath:database.properties")
@EnableMongoRepositories(
        basePackages = "pe.com.everest.transaccion")
public class MongoDB extends AbstractMongoConfiguration {
    @Value("${second.db.host}")
    private String host;
    @Value("${second.db.port}")
    private Integer port;
    @Value("${second.db.name}")
    private String dbname;
    @Value("${second.db.username}")
    private String username;
    @Value("${second.db.password}")
    private String password;
    @Value("${entitymanager.packages.to.scan}")
    private String packageScan;

    @Override
    protected String getDatabaseName() {
        return this.dbname;
    }

    @Bean
    public Mongo mongo() throws Exception {
        return new MongoClient(host, port);
    }

    @Bean
    public MongoTemplate mongoTemplate() throws Exception {
        MongoCredential mongoCredential = MongoCredential.createCredential(this.username, getDatabaseName(), this.password.toCharArray());
        ServerAddress address = new ServerAddress(this.host, this.port);
        MongoClient mongoClient = new MongoClient(address, Arrays.asList(mongoCredential));
        return new MongoTemplate(mongoClient, getDatabaseName());
    }
}
