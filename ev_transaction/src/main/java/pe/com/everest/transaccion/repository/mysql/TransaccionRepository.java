package pe.com.everest.transaccion.repository.mysql;

import pe.com.everest.transaccion.entity.TransaccionJPA;

import java.util.List;

public interface TransaccionRepository extends MySQLRepository<TransaccionJPA, Integer> {
    List<TransaccionJPA> findByAdtinstance(Integer adtInstance);
}
