package pe.com.everest.transaccion.enume;

import pe.com.everest.transaccion.exception.TransaccionException;

import java.util.Optional;
import java.util.stream.Stream;

import static pe.com.everest.transaccion.enume.MessageEnum.ADT_NOT_ALLOWED;

/**
 * Created by MHER on 8/18/2018.
 */
public enum ADTEnum {
    QUEUE(1),
    BINARYTREE(2);

    private Integer codigo;

    ADTEnum(Integer codigo) {
        this.codigo = codigo;
    }

    public static ADTEnum getEnum(Integer codigo) {
        Optional<ADTEnum> optional = Stream.of(values()).filter(a -> a.codigo.equals(codigo)).findFirst();
        if (!optional.isPresent())
            throw new TransaccionException(ADT_NOT_ALLOWED.mensaje(codigo));
        return optional.get();
    }
}
