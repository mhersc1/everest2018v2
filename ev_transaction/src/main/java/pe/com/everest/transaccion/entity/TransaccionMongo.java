package pe.com.everest.transaccion.entity;

import javax.persistence.Id;
import java.util.Date;

/**
 * Created by MHER on 8/17/2018.
 */
public class TransaccionMongo extends Transaccion {
    @Id
    @Override
    public Integer getId() {
        return super.getId();
    }

    @Override
    public void setId(Integer id) {
        super.setId(id);
    }

    @Override
    public String getNombre() {
        return super.getNombre();
    }

    @Override
    public void setNombre(String nombre) {
        super.setNombre(nombre);
    }

    @Override
    public Integer getPeso() {
        return super.getPeso();
    }

    @Override
    public void setPeso(Integer peso) {
        super.setPeso(peso);
    }

    @Override
    public String getTipo() {
        return super.getTipo();
    }

    @Override
    public void setTipo(String tipo) {
        super.setTipo(tipo);
    }

    @Override
    public Date getFecha() {
        return super.getFecha();
    }

    @Override
    public void setFecha(Date fecha) {
        super.setFecha(fecha);
    }
}
