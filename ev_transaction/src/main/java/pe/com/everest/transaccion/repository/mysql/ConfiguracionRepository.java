package pe.com.everest.transaccion.repository.mysql;

import pe.com.everest.transaccion.entity.ConfiguracionJPA;

/**
 * Created by MHER on 21/08/2018.
 */
public interface ConfiguracionRepository extends MySQLRepository<ConfiguracionJPA, String> {
}
