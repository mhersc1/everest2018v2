package pe.com.everest.transaccion.config;


import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pe.com.everest.transaccion.util.Util;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;


/**
 * Created by MHER on 8/18/2018.
 */
@Aspect
public class Logging {
    private static final Logger logger = LoggerFactory.getLogger(Logging.class);


    /**
     * Pointcut that matches all repositories, services and Web REST endpoints.
     */
    @Pointcut("within(pe.com.everest.transaccion.controller..*)"
            + "|| within(pe.com.everest.transaccion.service..*)"
            + "|| within(pe.com.everest.transaccion.dao..*)")
    public void loggingPointcut() {
        // Method is empty as this is just a Poincut, the implementations are in the advices.
    }

    /**
     * Advice that logs methods throwing exceptions.
     */
    @AfterThrowing(pointcut = "loggingPointcut()", throwing = "e")
    public void logAfterThrowing(JoinPoint joinPoint, Throwable e) {
        String signatureOfArgs = concatenateArgs(joinPoint.getArgs());
        logger.error(MessageFormat.format("Exception in {0}.{1}({2}) with cause = {3}", joinPoint.getSignature().getDeclaringTypeName(),
                joinPoint.getSignature().getName(), signatureOfArgs, Util.printStackTrace((Exception) e)));
    }

    /**
     * Advice that logs when a method is entered and exited.
     */
    @Around("loggingPointcut()")
    public Object logAroundEndpoint(ProceedingJoinPoint joinPoint) throws Throwable {
        String signatureOfArgs = concatenateArgs(joinPoint.getArgs());
        if (logger.isInfoEnabled()) {
            logger.info(MessageFormat.format("Inicio: {0}.{1}({2}) with argument[s] = {3}", joinPoint.getSignature().getDeclaringTypeName(),
                    joinPoint.getSignature().getName(), signatureOfArgs, Util.formatAsJson(joinPoint.getArgs())));
        }
        try {
            Object result = joinPoint.proceed();

            if (logger.isInfoEnabled()) {
                logger.info(MessageFormat.format("Fin: {0}.{1}({2}) with result = {3}", joinPoint.getSignature().getDeclaringTypeName(),
                        joinPoint.getSignature().getName(), signatureOfArgs, Util.formatAsJson(result)));
            }
            return result;
        } catch (IllegalArgumentException e) {
            logger.error(MessageFormat.format("Illegal argument: {0} in {1}.{2}()", Arrays.toString(joinPoint.getArgs()),
                    joinPoint.getSignature().getDeclaringTypeName(), joinPoint.getSignature().getName()));

            throw e;
        }
    }

    private String concatenateArgs(Object[] objects) {
        List<String> signatures = new ArrayList<>();
        Stream.of(objects).forEach(o -> signatures.add(o.getClass().getSimpleName()));
        return String.join(",", signatures);
    }
}
