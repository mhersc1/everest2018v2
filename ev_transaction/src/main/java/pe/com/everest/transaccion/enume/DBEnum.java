package pe.com.everest.transaccion.enume;

import java.util.stream.Stream;

/**
 * Created by MHER on 8/18/2018.
 */
public enum DBEnum {
    MYSQL,
    MONGO;

    public static boolean verifyEnum(String dbname) {
        return Stream.of(values()).filter(e -> e.equals(valueOf(dbname))).count() > 0;
    }
}
