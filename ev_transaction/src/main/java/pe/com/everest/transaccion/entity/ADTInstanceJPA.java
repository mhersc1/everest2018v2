package pe.com.everest.transaccion.entity;

import javax.persistence.*;

@Entity
@Table(name = "instanceadt")
public class ADTInstanceJPA extends ADTInstance {

    public ADTInstanceJPA(Integer tipo) {
        super(tipo);
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Override
    public Integer getId() {
        return super.getId();
    }

    @Override
    public void setId(Integer id) {
        super.setId(id);
    }

    @Override
    public Integer getTipo() {
        return super.getTipo();
    }

    @Override
    public void setTipo(Integer tipo) {
        super.setTipo(tipo);
    }

    @Override
    public String getNombre() {
        return super.getNombre();
    }

    @Override
    public void setNombre(String nombre) {
        super.setNombre(nombre);
    }

    @Override
    public String toString() {
        return super.toString();
    }
}