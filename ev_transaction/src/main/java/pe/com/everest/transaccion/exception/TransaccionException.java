package pe.com.everest.transaccion.exception;

/**
 * Created by MHER
 */
public class TransaccionException extends RuntimeException {
    public TransaccionException(String message) {
        super(message);
    }
}