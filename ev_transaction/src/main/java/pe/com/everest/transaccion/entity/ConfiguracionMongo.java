package pe.com.everest.transaccion.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by MHER on 21/08/2018.
 */
@Document(collection = "configuracion")
public class ConfiguracionMongo extends Configuracion {

    public ConfiguracionMongo(String clave, String valor) {
        super(clave, valor);
    }

    @Id
    @Override
    public String getClave() {
        return super.getClave();
    }

    @Override
    public void setClave(String clave) {
        super.setClave(clave);
    }

    @Override
    public String getValor() {
        return super.getValor();
    }

    @Override
    public void setValor(String valor) {
        super.setValor(valor);
    }
}
