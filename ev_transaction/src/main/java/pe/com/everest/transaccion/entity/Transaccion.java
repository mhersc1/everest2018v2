package pe.com.everest.transaccion.entity;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.util.Date;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Transaccion implements Serializable {

    private Integer id;
    private String nombre;
    private Integer peso;
    private String tipo;
    private Date fecha;
    private Integer adtinstance;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getPeso() {
        return peso;
    }

    public void setPeso(Integer peso) {
        this.peso = peso;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Integer getAdtinstance() {
        return adtinstance;
    }

    public void setAdtinstance(Integer adtinstance) {
        this.adtinstance = adtinstance;
    }

    @Override
    public String toString() {
        return "Transaccion{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", peso=" + peso +
                ", tipo='" + tipo + '\'' +
                ", fecha=" + fecha +
                ", adtinstance=" + adtinstance +
                '}';
    }
}
