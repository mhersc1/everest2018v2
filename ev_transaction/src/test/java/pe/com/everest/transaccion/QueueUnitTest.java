package pe.com.everest.transaccion;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;
import pe.com.everest.transaccion.component.GenericBuilder;
import pe.com.everest.transaccion.component.Queue;
import pe.com.everest.transaccion.entity.Transaccion;

import java.util.Date;

@RunWith(SpringRunner.class)
public class QueueUnitTest {
    private static final Logger logger = LoggerFactory.getLogger(QueueUnitTest.class);

    @TestConfiguration
    static class ReferenceServiceTestContextConfiguration {
        @Bean
        public Queue queue() {
            return new Queue();
        }
    }

    @Autowired
    private Queue queue;

    @Before
    public void init() {
        queue.enqueue(createTransaccion(2));
        queue.enqueue(createTransaccion(3));
        queue.enqueue(createTransaccion(4));
        queue.enqueue(createTransaccion(5));
    }

    @Test
    public void enqueue() {
        queue.enqueue(createTransaccion(6));
        logger.debug(queue.toString());
    }

    @Test
    public void dequeue() {
        queue.dequeue();
        logger.debug(queue.toString());
    }

    @Test
    public void peek() {
        Transaccion transaccion = queue.peek();
        logger.debug(transaccion.toString());
    }

    @Test
    public void isEmpty() {
        Assert.assertTrue("La cola esta vacía!", !queue.isEmpty());
    }

    @Test
    public void search() {
        logger.info(queue.search(createTransaccion(4)).toString());
    }

    @Test
    public void deploy() {
        logger.debug(queue.toString());
    }

    private static Transaccion createTransaccion(int peso) {
        return GenericBuilder.of(Transaccion::new)
                .with(Transaccion::setNombre, "Cambio de cheques del area de contabilidad. ")
                .with(Transaccion::setFecha, new Date())
                .with(Transaccion::setPeso, peso)
                .with(Transaccion::setTipo, "CCH")
                .with(Transaccion::setAdtinstance, 1)
                .build();

    }
}
