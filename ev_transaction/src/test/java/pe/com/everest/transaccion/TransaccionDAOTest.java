package pe.com.everest.transaccion;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;
import pe.com.everest.transaccion.component.GenericBuilder;
import pe.com.everest.transaccion.config.MongoDB;
import pe.com.everest.transaccion.config.MySQLDB;
import pe.com.everest.transaccion.dao.TransaccionDAO;
import pe.com.everest.transaccion.entity.ADTInstance;
import pe.com.everest.transaccion.entity.Transaccion;
import pe.com.everest.transaccion.enume.DBEnum;
import pe.com.everest.transaccion.util.Util;

import java.util.Date;

@RunWith(SpringRunner.class)
@Import({MongoDB.class, MySQLDB.class})
public class TransaccionDAOTest {

    @Before
    public void init() throws Exception {
        Util.injectDataBaseInRunTime(DBEnum.MONGO.toString());
    }

    @TestConfiguration
    static class TransaccionServiceTestConfiguration {
        @Bean
        TransaccionDAO transaccionDAO() {
            return new TransaccionDAO();
        }
    }

    @Autowired
    TransaccionDAO transaccionDAO;


    @Test
    public void saveADT() {
        ADTInstance adtInstance = new ADTInstance(3);
        transaccionDAO.saveADT(adtInstance);
    }

    @Test
    public void saveTrxIntoADT() {
        Transaccion transaccion = GenericBuilder.of(Transaccion::new)
                .with(Transaccion::setNombre, "Cambio de cheques del area de contabilidad. ")
                .with(Transaccion::setFecha, new Date())
                .with(Transaccion::setPeso, 100)
                .with(Transaccion::setTipo, "CCH")
                .with(Transaccion::setAdtinstance, 1)
                .build();

        transaccionDAO.registrarTrx(transaccion);
    }

    @Test
    public void deleteTrxIntoADT() {
        Transaccion transaccion = GenericBuilder.of(Transaccion::new)
                .with(Transaccion::setId, 7)
                .with(Transaccion::setNombre, "Cambio de cheques del area de contabilidad. ")
                .with(Transaccion::setFecha, new Date())
                .with(Transaccion::setPeso, 100)
                .with(Transaccion::setTipo, "CCH")
                .with(Transaccion::setAdtinstance, 1)
                .build();

        transaccionDAO.eliminarTrx(transaccion);
    }
}
