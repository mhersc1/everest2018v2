package pe.com.everest.transaccion;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;
import pe.com.everest.transaccion.component.ArbolBinario;
import pe.com.everest.transaccion.component.GenericBuilder;
import pe.com.everest.transaccion.entity.Transaccion;

import java.util.Date;

@RunWith(SpringRunner.class)
public class ArbolBinarioTest {
    private static final Logger logger = LoggerFactory.getLogger(ArbolBinarioTest.class);

    @TestConfiguration
    static class ArbolBinarioTestContextConfiguration {
        @Bean
        public ArbolBinario arbolBinario() {
            return new ArbolBinario();
        }
    }

    @Autowired
    private ArbolBinario arbolBinario;

    @Before
    public void init() {
        arbolBinario.insert(createTransaccion(5));
        arbolBinario.insert(createTransaccion(1));
        arbolBinario.insert(createTransaccion(6));
        arbolBinario.insert(createTransaccion(8));
        arbolBinario.insert(createTransaccion(4));
    }

    @Test
    public void insertar() {
        arbolBinario.insert(createTransaccion(3));
        arbolBinario.listar().forEach(t -> logger.info(t.toString()));
    }

    @Test
    public void borrar() {
        arbolBinario.eliminar(createTransaccion(1));
        arbolBinario.listar().forEach(t -> logger.info(t.toString()));
    }

    @Test
    public void buscar() {
        logger.info(arbolBinario.search(createTransaccion(6)).toString());
    }

    @Test
    public void desplegar() {
        arbolBinario.listar().forEach(t -> logger.info(t.toString()));
    }


    private static Transaccion createTransaccion(int peso) {
        return GenericBuilder.of(Transaccion::new)
                .with(Transaccion::setNombre, "Cambio de cheques del area de contabilidad. ")
                .with(Transaccion::setFecha, new Date())
                .with(Transaccion::setPeso, peso)
                .with(Transaccion::setTipo, "CCH")
                .with(Transaccion::setAdtinstance, 1)
                .build();

    }
}
