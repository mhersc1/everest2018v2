package pe.com.everest.transaccion;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;
import pe.com.everest.transaccion.component.GenericBuilder;
import pe.com.everest.transaccion.config.MongoDB;
import pe.com.everest.transaccion.entity.ADTInstanceMongo;
import pe.com.everest.transaccion.entity.Transaccion;
import pe.com.everest.transaccion.entity.TransaccionMongo;
import pe.com.everest.transaccion.repository.mongo.ADTDocumentRepository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by MHER on 8/18/2018.
 */
@RunWith(SpringRunner.class)
@Import(value = MongoDB.class)
public class TransactionRepositoryMongoTest {
    @Autowired
    private ADTDocumentRepository adtDocumentRepository;

    @Test
    public void saveNoSQLTransaction() {
        ADTInstanceMongo adtInstanceMongo = new ADTInstanceMongo(1, 1);
        adtInstanceMongo.setNombre("ARBOL_0002");
        List<TransaccionMongo> transacciones = new ArrayList<>();
        transacciones.add(createTransaccion(1));
        transacciones.add(createTransaccion(2));
        transacciones.add(createTransaccion(3));
        transacciones.add(createTransaccion(4));
        transacciones.add(createTransaccion(5));
        adtInstanceMongo.getTransaccionList().addAll(transacciones);
        adtDocumentRepository.save(adtInstanceMongo);
    }

    private static TransaccionMongo createTransaccion(int peso) {
        return GenericBuilder.of(TransaccionMongo::new)
                //.with(Transaccion::setId, 1)
                .with(Transaccion::setNombre, "Cambio de cheques del area de contabilidad. ")
                .with(Transaccion::setFecha, new Date())
                .with(Transaccion::setPeso, peso)
                .with(Transaccion::setTipo, "CCH")
                .build();

    }
}
