package pe.com.everest.transaccion;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace= AutoConfigureTestDatabase.Replace.NONE)
public class ApplicationTests {
	
	private static final Logger logger = LoggerFactory.getLogger(ApplicationTests.class);

	@TestConfiguration
	static class EntityTestContextConfiguration{

	}
	@Test
	public void example(){
		logger.info("It's mandatory at least to put one method annotated with Test");
	}
}