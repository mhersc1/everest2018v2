package pe.com.everest.transaccion;

import pe.com.everest.transaccion.util.Util;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * Created by MHER_ on 09/12/2017.
 */
public class UtilTest {
    private static final Logger logger = LoggerFactory.getLogger(ApplicationTests.class);

    @Test
    public void test() {
        logger.info("Hoy: " + Util.formatHHMM(new Date()));
        logger.info("Hora: " + Util.obtainHour("06:30 PM"));
    }

    @Test
    public void test2() {
        logger.info("Hoy: " + Util.formatHHMM(new Date()));
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("hh:mm a");
        Date date = new Date();
        LocalTime ahora = Util.obtainHour("11:59 AM");
        LocalTime horaInicio = Util.obtainHour("12:00 PM");
        LocalTime horaFin = Util.obtainHour("12:10 PM");

        if (ahora.isBefore(horaInicio) || ahora.isAfter(horaFin)) {
            logger.info("Hora Registro: " + ahora.format(formatter) + " no permitida ");
        }
    }
}
