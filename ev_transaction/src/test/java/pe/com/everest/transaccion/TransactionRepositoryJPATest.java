package pe.com.everest.transaccion;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;
import pe.com.everest.transaccion.component.GenericBuilder;
import pe.com.everest.transaccion.config.MySQLDB;
import pe.com.everest.transaccion.entity.ADTInstanceJPA;
import pe.com.everest.transaccion.entity.Transaccion;
import pe.com.everest.transaccion.entity.TransaccionJPA;
import pe.com.everest.transaccion.repository.mysql.ADTInstanceRepository;
import pe.com.everest.transaccion.repository.mysql.TransaccionRepository;

import java.util.Date;

@RunWith(SpringRunner.class)
@Import(MySQLDB.class)
public class TransactionRepositoryJPATest {
    private static final Logger logger = LoggerFactory.getLogger(TransactionRepositoryJPATest.class);
    @Autowired
    private ADTInstanceRepository adtInstanceRepository;

    @Autowired
    private TransaccionRepository transaccionRepository;


    @Test
    //@Rollback(false)
    public void saveSQLTransaction() {
        ADTInstanceJPA adtInstanceJPA = new ADTInstanceJPA(1);
        adtInstanceRepository.save(adtInstanceJPA);
        logger.info(adtInstanceJPA.toString());

        TransaccionJPA transaccion = GenericBuilder.of(TransaccionJPA::new)
                .with(Transaccion::setNombre, "Cambio de cheques del area de contabilidad. ")
                .with(Transaccion::setFecha, new Date())
                .with(Transaccion::setPeso, 10)
                .with(Transaccion::setTipo, "CCH")
                .with(Transaccion::setAdtinstance, adtInstanceJPA.getId())
                .build();

        transaccionRepository.save(transaccion);
        logger.info(transaccion.toString());
    }
}
