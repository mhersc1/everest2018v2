delimiter //
CREATE TRIGGER ev_transaccion.trigger_instance_trx BEFORE INSERT
    ON ev_transaccion.instanceadt FOR EACH ROW 
BEGIN    	
SET NEW.identificador = (SELECT COALESCE(MAX(identificador),0) FROM ev_transaccion.instanceadt) + 1;
SET NEW.nombre = (SELECT CONCAT(descripcion,'_',LPAD(NEW.identificador,5,'0')) FROM ev_transaccion.adt WHERE identificador = NEW.tipo);
END;//
delimiter ;

show triggers;
SHOW VARIABLES LIKE "%version%";

